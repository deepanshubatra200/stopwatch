import React from "react";
import { View, StyleSheet, Text, ScrollView } from "react-native";
import { useSelector } from "react-redux";
function Laps() {
  const LapArray = useSelector((state) => state.timer.lapTime);

  return (
    <ScrollView style={styles.scrollView}>
      {LapArray.map((time) => (
       
          <View style={styles.lap} key={time.key}>
            <Text style={styles.text} >
              {time.timelap}
            </Text>
          
        </View>
      ))}
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "pink",
    marginHorizontal: 20,
    borderRadius:7
  },
  lap: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: "wheat",
    borderRadius: 6,
    alignItems: "center",
  },
  text: {
    alignSelf:'center'
  },
});
export default Laps;
