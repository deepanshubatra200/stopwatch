import React from "react";
import { timerActions } from "../../store/store";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet, TouchableOpacity,Text } from "react-native";
function Buttons() {
  const dispatch = useDispatch();
  const isPaused = useSelector((state) => state.timer.isPaused);
  const StartHandler = () => {
    dispatch(timerActions.handleStart());
  };
  const pauseHandler = () => {
    dispatch(timerActions.handlePauseResume());
  };
  const handleReset = () => {
    dispatch(timerActions.handleReset());
  };
  const lapHandler = () => {
    dispatch(timerActions.setLap());
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={StartHandler} style={styles.button}>
        <Text>Start</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={pauseHandler} style={styles.button}>
        <Text>{isPaused ? "Resume" : "Pause"}</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={handleReset} style={styles.button}>
        <Text>Reset</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={lapHandler} style={styles.button}>
        <Text>Laps</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        justifyContent: 'center',
        paddingHorizontal: 10,
        justifyContent:'space-around',
      },
      button: {
        alignItems: 'center',
        backgroundColor: 'pink',
        padding: 10,
        marginBottom:5,
        borderRadius:50
      },
});

export default Buttons;
