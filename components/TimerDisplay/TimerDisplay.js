import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { timerActions } from "../../store/store";


function TimerDisplay() {
  const time = useSelector((state) => state.timer.time);
  const dispatch = useDispatch();

  const currentTime =
    ("0" + Math.floor((time / 360000) % 60)).slice(-2) +
    " : " +
    ("0" + Math.floor((time / 60000) % 60)).slice(-2) +
    " :  " +
    ("0" + Math.floor((time / 1000) % 60)).slice(-2) +
    " : " +
    ("0" + ((time / 10) % 100)).slice(-2);
  dispatch(timerActions.setCurrentTime(currentTime));
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{currentTime}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    margin: 20,
    backgroundColor: "orange",
    padding: 20,
    borderRadius: 10,
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default TimerDisplay;
