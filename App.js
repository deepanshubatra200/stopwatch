import React,{useEffect} from "react";
import { View, StyleSheet, Text } from "react-native";
import { useDispatch, useSelector, Provider } from "react-redux";
import Buttons from "./components/Button/Button";
import Laps from "./components/Laps/Laps";
import store, { timerActions } from "./store/store";
import TimerDisplay from "./components/TimerDisplay/TimerDisplay";

function App() {
  const isActive = useSelector((state) => state.timer.isActive);
  const isPaused = useSelector((state) => state.timer.isPaused);

  const dispatch = useDispatch();
  useEffect(() => {
    let interval = null;

    if (isActive && isPaused === false) {
      interval = setInterval(() => {
        dispatch(timerActions.setTimer(10));
      }, 10);
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive, isPaused, dispatch]);

  return (
     <View style={styles.container}>
        <TimerDisplay />
        <Buttons />
        <Laps />
      </View>
  );
}

const styles = StyleSheet.create({
  container : {
    top:50 ,
    backgroundColor:'black',
    maxHeight:'95%'
  }
});

export default ()=>{
  return (
    <Provider store={store}>
      <App/>
     </Provider>
  )
}