import {createSlice,configureStore} from '@reduxjs/toolkit'


const initialState={
    isActive :false , 
    isPaused:true ,
    time:0 ,
    currentTime:"",
    lapTime:[]
}

const timerSlice=createSlice({
    name:"timer",
    initialState,
    reducers : {
        setTimer(state,action){
            state.time=state.time+action.payload;
        },
        handleStart(state){
           
            state.isActive=true;
            state.isPaused=false;
        },
        handlePauseResume(state){
           
            state.isPaused=!state.isPaused
        },
        handleReset(state){
            
            state.isActive=false;
            state.time=0
            state.lapTime=[]
        },
        setCurrentTime(state,action){
                state.currentTime=action.payload
        }
        ,
        setLap(state){
            
            state.lapTime.push({key : state.currentTime.toString(),timelap:state.currentTime})
            
        }
    }
})

const store=configureStore({
    reducer : {timer :  timerSlice.reducer}
})

export const timerActions=timerSlice.actions;
export default store;